package banking;

public abstract class Bank {
	
	private String address;
	//methods abstract and also non-abstract methods
	
	public Bank(String address) {
		setAddress(address);
	}
	
	public abstract double getInterestRate(); // abstract methods does not have body
	
	public String getAddress() {
		return this.address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
}
