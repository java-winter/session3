package banking;

public class RbcBank extends Bank {

	public RbcBank(String address) {
		super(address);
	}

	@Override
	public double getInterestRate() {
		return 2.25;
	}

}
