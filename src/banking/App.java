package banking;

public class App {

	public static void main(String[] args) {
		//BAnk
		//RbcBank
		//TDBank
		
		
		//Bank bank = new Bank("asdfasd"); abstract class cannot be instantiated
		
		Bank rbcBank = new RbcBank("Main street");
		Bank cibcBank = new CIbcBank("second street");
		//polymorphism
		
		generateInterestRateReports(rbcBank);
		generateInterestRateReports(cibcBank);
		
	}
	
	
	public static void generateInterestRateReports(Bank bank) {
		System.out.println(bank.getInterestRate());
	}

}
