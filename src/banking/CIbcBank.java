package banking;

public class CIbcBank extends Bank {
	
	public CIbcBank(String address) {
		super(address);
	}

	@Override
	public double getInterestRate() {
		return 2.29;
	}

}
