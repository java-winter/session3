package association1to1;

public class Passport {
	private String passNo;
	private Person p;
	
	public Passport(String passNo, Person p) {
		this.p = p;
		this.passNo = passNo;
	}

	@Override
	public String toString() {
		return "Passport [passNo=" + passNo + ", p=" + p + "]";
	}
	
	
}
