package association;

public class App {
	
	public static void main(String[] args) {
		Province province = new Province();
		province.setProvinceName("QC");
		
		City city1 = new City();
		city1.setCityName("Montreal");
		
		City city2 = new City();
		city2.setCityName("Laval");
		
		City city3 = new City();
		city3.setCityName("Longueuil");
		
		City[] cities = new City[3];
		cities[0] =city1;
		cities[1] =city2;
		cities[2] =city3;
		
		province.setCities(cities);
		
		System.out.println(province);
		
		//person and passport => 1 to 1
		
		//car and owner => 1 to many
	}
}
