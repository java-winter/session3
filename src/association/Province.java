package association;

import java.util.Arrays;

public class Province {
	private String provinceName;

	//add association one to many
	//List<City> cities;
	private City[] cities;
	
	
	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public City[] getCities() {
		return cities;
	}

	public void setCities(City[] cities) {
		this.cities = cities;
	}

	@Override
	public String toString() {
		return "Province [provinceName=" + provinceName + ", cities=" + Arrays.toString(cities) + "]";
	}
	
	
	
	
}
