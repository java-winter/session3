package aggregation;

public class Address {
	private int buildingNo;
	private String streetName;
	private String cityName;
	private String provinceName;
	
	public Address(int buildingNo, String streetName, String cityName, String provinceName) {
	
		this.buildingNo = buildingNo;
		this.streetName = streetName;
		this.cityName = cityName;
		this.provinceName = provinceName;
	}
	
	
	
}
