package aggregation;

public class Student {
	private String firstName;
	private String lastName;
	
	private int stdNo;
	
	//address //aggregation
	private Address address;

	public Student(String firstName, String lastName, int stdNo, Address address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.stdNo = stdNo;
		this.address = address;
	}
	
	
	
}
