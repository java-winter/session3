package aggregation;

public class Teacher {
	private String firstName;
	private String lastName;
	
	private int employeeNo;
	
	//address
	private Address address;

	public Teacher(String firstName, String lastName, int employeeNo) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.employeeNo = employeeNo;
	}

	public Teacher(String firstName, String lastName, int employeeNo, Address address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.employeeNo = employeeNo;
		this.address = address;
	}
	

}
